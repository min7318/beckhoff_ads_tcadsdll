/**
   Copyright (c) 2015 Beckhoff Automation GmbH & Co. KG

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
 */
#include "AdsLib.h"
#include "AmsRouter.h"
#include <pthread.h>
#include "TcAdsDll.h"

#ifdef __cplusplus
extern "C"
{
#endif
AmsRouter& GetRouter();

#ifdef __cplusplus
} // extern "C"
#endif

static int32_t _lastport=0;

int32_t AdsGetDllVersion(void)
{
    return 0x250000|0x20;
}

int32_t AdsPortOpen(void)
{
    if(!_lastport)
    {
        _lastport = AdsPortOpenEx();
    }
    return _lastport;
}

int32_t AdsPortClose(void)
{
    int32_t ret= AdsPortCloseEx(_lastport);
    _lastport=0;
    return ret;
}

int32_t AdsGetLocalAddress(AmsNetId* ams)
{
    AmsAddr addr;
    AmsAddr* paddr=&addr;
    int32_t ret; 
    ret = AdsGetLocalAddressEx(_lastport, paddr);
    memcpy(ams,&addr,sizeof(AmsNetId));
    return ret;
}

int32_t AdsSyncReadReqEx( AmsAddr*	pAddr,			// Ams address of ADS server           
			 uint32_t	indexGroup,		// index group in ADS server interface
			 uint32_t	indexOffset,	// index offset in ADS server interface
			 uint32_t	length,			// count of bytes to read
			 void*		pData,			// pointer to the client buffer
			 uint32_t*	pcbReturn		// count of bytes read
							)
{
    return AdsSyncReadReqEx2(_lastport,pAddr,indexGroup,indexOffset,length,
                             pData,pcbReturn);
}

int32_t AdsSyncReadReq( AmsAddr*			pAddr,			// Ams address of ADS server           
			 uint32_t		indexGroup,		// index group in ADS server interface
			 uint32_t		indexOffset,	// index offset in ADS server interface
			 uint32_t		length,			// count of bytes to read
			 void*				pData			// pointer to the client buffer
			)
{
    uint32_t read;
    uint32_t* pread=&read;
    return AdsSyncReadReqEx(pAddr,indexGroup,
                            indexOffset,length,pData,pread);
}

int32_t AdsSyncWriteReq(AmsAddr*		pServerAddr,	// Ams address of ADS server
			 uint32_t		indexGroup,		// index group in ADS server interface
			 uint32_t		indexOffset,	// index offset in ADS server interface
			 uint32_t		length,			// count of bytes to write
			 void*				pData			// pointer to the client buffer
			)
{
    return AdsSyncWriteReqEx(_lastport,pServerAddr,indexGroup,indexOffset,
                             length,pData);
}

int32_t AdsSyncReadWriteReqEx( AmsAddr* pAddr,				// Ams address of ADS server
				uint32_t		indexGroup,		// index group in ADS server interface
				uint32_t 		indexOffset,	// index offset in ADS server interface
				uint32_t 		cbReadLength,	// count of bytes to read
				void* 				pReadData,		// pointer to the client buffer
				uint32_t 		cbWriteLength,	// count of bytes to write
				void* 				pWriteData,		// pointer to the client buffer
				uint32_t*		pcbReturn
				)
{
    return AdsSyncReadWriteReqEx2(_lastport,pAddr,indexGroup,indexOffset,
                                  cbReadLength,pReadData,cbWriteLength,
                                  pWriteData,pcbReturn);
}

int32_t AdsSyncReadWriteReq( AmsAddr* pAddr,				// Ams address of ADS server
				uint32_t		indexGroup,		// index group in ADS server interface
				uint32_t 		indexOffset,	// index offset in ADS server interface
				uint32_t 		cbReadLength,	// count of bytes to read
				void* 				pReadData,		// pointer to the client buffer
				uint32_t 		cbWriteLength,	// count of bytes to write
				void* 				pWriteData		// pointer to the client buffer
				)
{
    uint32_t read;
    uint32_t* pread=&read;
    return AdsSyncReadWriteReqEx(pAddr,indexGroup,indexOffset,cbReadLength,pReadData,cbWriteLength,
                                 pWriteData,pread);
}

int32_t AdsSyncReadDeviceInfoReq( AmsAddr*		pAddr,		// Ams address of ADS server 
				 char*			pDevName,	// fixed length string (16 Byte)
				 AdsVersion*	pVersion	// client buffer to store server version
				 )
{
    return AdsSyncReadDeviceInfoReqEx(_lastport,pAddr,pDevName,pVersion);
}
int32_t AdsSyncWriteControlReq( AmsAddr*			pAddr,		// Ams address of ADS server           
				  uint16_t	adsState,	// index group in ADS server interface 
				  uint16_t	deviceState,// index offset in ADS server interface
				  uint32_t		length,		// count of bytes to write
				  void*				pData		// pointer to the client buffer        
				  )
{
    return AdsSyncWriteControlReqEx(_lastport,pAddr,adsState,deviceState,
                                    length,pData);
}

int32_t AdsSyncReadStateReq( AmsAddr*		pAddr,			// Ams address of ADS server           
			  uint16_t*	pAdsState,		// pointer to client buffer
			  uint16_t*	pDeviceState	// pointer to the client buffer        
			  )
{
    return AdsSyncReadStateReqEx(_lastport,pAddr,pAdsState,pDeviceState);
}

int32_t AdsSyncAddDeviceNotificationReq(	AmsAddr*				pAddr, 			// Ams address of ADS server           
					uint32_t			indexGroup,		// index group in ADS server interface
					uint32_t			indexOffset,	// index offset in ADS server interface
					AdsNotificationAttrib*	pNoteAttrib,	// attributes of notification request
					PAdsNotificationFuncEx	pNoteFunc,		// address of notification callback
					uint32_t			hUser,			// user handle
					uint32_t			*pNotification	// pointer to notification handle (return value)
					)
{
    return AdsSyncAddDeviceNotificationReqEx(_lastport,
                             pAddr,indexGroup,indexOffset,pNoteAttrib,
                             pNoteFunc,hUser,pNotification);
}

int32_t AdsSyncDelDeviceNotificationReq(	AmsAddr*		pAddr,			// Ams address of ADS server            
						uint32_t	hNotification	// notification handle
					)
{
    return AdsSyncDelDeviceNotificationReqEx(_lastport, pAddr,hNotification);

}
int32_t AdsSyncSetTimeout( int32_t nMs )
{
    return AdsSyncSetTimeoutEx(_lastport,nMs);
}

///typedef void  ( __attribute__ ((stdcall))  *PAmsRouterNotificationFuncEx)( int32_t nEvent );

int32_t AdsAmsRegisterRouterNotification ( PAmsRouterNotificationFuncEx pNoteFunc )
{
    return GetRouter().RegisterRouterNotification(pNoteFunc);
}

int32_t AdsAmsUnRegisterRouterNotification ()
{
    return GetRouter().UnRegisterRouterNotification();
}

int32_t AdsSyncGetTimeout( int32_t *pnMs )
{
    return AdsSyncGetTimeoutEx(_lastport,pnMs);
}

int32_t AdsAmsPortEnabled( int32_t *pbEnabled)
{
    *pbEnabled = GetRouter().IsOpen(_lastport)?1:0;
    
    return 0;
}

int32_t AdsAmsPortEnabledEx(int32_t port, int32_t *pbEnabled)
{
    *pbEnabled = GetRouter().IsOpen(port)?1:0;
    return 0;
}

int32_t AdsRouteAdd(const void* ams, const char* ip)
{
    AmsNetId* pams=(AmsNetId *)ams; 
    return AdsAddRoute(*pams, ip);
}

int32_t AdsRouteDel(const void* ams)
{
    AmsNetId* pams=(AmsNetId *) ams;
    AdsDelRoute(*pams);
    return 0 ;
}
int32_t AdsRouteSetLocal(const void* ams)
{
    AmsNetId* pams=(AmsNetId *) ams;
    AdsSetLocalAddress(*pams);
    return 0;
}

