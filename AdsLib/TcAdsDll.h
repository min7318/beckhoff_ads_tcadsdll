#ifndef __TCADSDLL_H__
#define __TCADSDLL_H__

#include "AdsDef.h"


////extern "C" void __cxa_pure_virtual(void ) {while(1);}
////////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C"
{
#endif

__attribute__((visibility ("default"))) 
int32_t AdsGetDllVersion( void );

__attribute__ ((visibility ("default")))
int32_t	 AdsPortOpen( void ); 

__attribute__ ((visibility ("default")))
int32_t	 AdsPortClose( void );

__attribute__ ((visibility ("default")))
int32_t	 AdsGetLocalAddress( AmsNetId*	ams );
 
__attribute__ ((visibility ("default")))
int32_t  AdsSyncWriteReq(	AmsAddr*		pServerAddr,	// Ams address of ADS server
							 uint32_t		indexGroup,		// index group in ADS server interface
							 uint32_t		indexOffset,	// index offset in ADS server interface
							 uint32_t		length,			// count of bytes to write
							 void*				pData			// pointer to the client buffer
							);

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadReq( AmsAddr*			pAddr,			// Ams address of ADS server           
							 uint32_t		indexGroup,		// index group in ADS server interface
							 uint32_t		indexOffset,	// index offset in ADS server interface
							 uint32_t		length,			// count of bytes to read
							 void*				pData			// pointer to the client buffer
							);

__attribute__ ((visibility ("default")))
int32_t AdsSyncReadReqEx( AmsAddr*		pAddr,			// Ams address of ADS server           
							 uint32_t		indexGroup,		// index group in ADS server interface
							 uint32_t		indexOffset,	// index offset in ADS server interface
							 uint32_t		length,			// count of bytes to read
							 void*				pData,			// pointer to the client buffer
							 uint32_t*		pcbReturn		// count of bytes read
							);

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadWriteReq( AmsAddr* pAddr,				// Ams address of ADS server
							uint32_t		indexGroup,		// index group in ADS server interface
							uint32_t 		indexOffset,	// index offset in ADS server interface
							uint32_t 		cbReadLength,	// count of bytes to read
							void* 				pReadData,		// pointer to the client buffer
							uint32_t 		cbWriteLength,	// count of bytes to write
							void* 				pWriteData		// pointer to the client buffer
							);											

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadWriteReqEx( AmsAddr*	pAddr,			// Ams address of ADS server
							uint32_t		indexGroup,		// index group in ADS server interface
							uint32_t		indexOffset,	// index offset in ADS server interface
							uint32_t		cbReadLength,	// count of bytes to read
							void*				pReadData,		// pointer to the client buffer
							uint32_t		cbWriteLength,	// count of bytes to write
							void*				pWriteData,		// pointer to the client buffer
							uint32_t*		pcbReturn		// count of bytes read
							);											

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadDeviceInfoReq( AmsAddr*		pAddr,		// Ams address of ADS server 
										 char*			pDevName,	// fixed length string (16 Byte)
										 AdsVersion*	pVersion	// client buffer to store server version
										 );

__attribute__ ((visibility ("default")))
int32_t  AdsSyncWriteControlReq( AmsAddr*			pAddr,		// Ams address of ADS server           
									  uint16_t	adsState,	// index group in ADS server interface 
									  uint16_t	deviceState,// index offset in ADS server interface
									  uint32_t		length,		// count of bytes to write
									  void*				pData		// pointer to the client buffer        
									  );

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadStateReq( AmsAddr*		pAddr,			// Ams address of ADS server           
								  uint16_t*	pAdsState,		// pointer to client buffer
								  uint16_t*	pDeviceState	// pointer to the client buffer        
								  );							          

__attribute__ ((visibility ("default")))
int32_t  AdsSyncAddDeviceNotificationReq(	AmsAddr*				pAddr, 			// Ams address of ADS server           
												uint32_t			indexGroup,		// index group in ADS server interface
												uint32_t			indexOffset,	// index offset in ADS server interface
												AdsNotificationAttrib*	pNoteAttrib,	// attributes of notification request
												PAdsNotificationFuncEx	pNoteFunc,		// address of notification callback
												uint32_t			hUser,			// user handle
												uint32_t			*pNotification	// pointer to notification handle (return value)
												);

__attribute__ ((visibility ("default")))
int32_t  AdsSyncDelDeviceNotificationReq(	AmsAddr*		pAddr,			// Ams address of ADS server            
												uint32_t	hNotification	// notification handle
												);

__attribute__ ((visibility ("default")))
int32_t  AdsSyncSetTimeout( int32_t nMs );

__attribute__ ((visibility ("default")))
int32_t  AdsGetLastError( void ); 

/// register callback
__attribute__ ((visibility ("default")))
int32_t  AdsAmsRegisterRouterNotification ( PAmsRouterNotificationFuncEx pNoteFunc );

/// unregister callback
__attribute__ ((visibility ("default")))
int32_t  AdsAmsUnRegisterRouterNotification ();


__attribute__ ((visibility ("default")))
int32_t  AdsSyncGetTimeout( int32_t *pnMs );

__attribute__ ((visibility ("default")))
int32_t  AdsAmsPortEnabled( int32_t *pbEnabled);

////////////////////////////////////////////////////////////////////////////////////////////////////
// new Ads functions for multithreading applications
__attribute__ ((visibility ("default")))
int32_t  AdsPortOpenEx(void ); 

__attribute__ ((visibility ("default")))
int32_t	 AdsPortCloseEx( int32_t port );

__attribute__ ((visibility ("default")))
int32_t	 AdsGetLocalAddressEx( int32_t port, AmsAddr*	pAddr );
 
__attribute__ ((visibility ("default")))
int32_t  AdsSyncWriteReqEx( int32_t			port,			// Ams port of ADS client
								AmsAddr*		pServerAddr,	// Ams address of ADS server
								uint32_t	indexGroup,		// index group in ADS server interface
								uint32_t	indexOffset,	// index offset in ADS server interface
								uint32_t	length,			// count of bytes to write
								void*			pData			// pointer to the client buffer
								);

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadReqEx2( int32_t	port,					// Ams port of ADS client
							 AmsAddr*			pServerAddr,	// Ams address of ADS server
							 uint32_t		indexGroup,		// index group in ADS server interface
							 uint32_t		indexOffset,	// index offset in ADS server interface
							 uint32_t		length,			// count of bytes to read
							 void*				pData,			// pointer to the client buffer
							 uint32_t*		pcbReturn		// count of bytes read
							);


__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadWriteReqEx2( int32_t	port,			// Ams port of ADS client
							AmsAddr*			pServerAddr,	// Ams address of ADS server
							uint32_t		indexGroup,		// index group in ADS server interface
							uint32_t		indexOffset,	// index offset in ADS server interface
							uint32_t		cbReadLength,	// count of bytes to read
							void*				pReadData,		// pointer to the client buffer
							uint32_t		cbWriteLength,	// count of bytes to write
							void*				pWriteData,		// pointer to the client buffer
							uint32_t*		pcbReturn		// count of bytes read
							);											

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadDeviceInfoReqEx( int32_t		port,			// Ams port of ADS client
										 AmsAddr*		pServerAddr,	// Ams address of ADS server
										 char*			pDevName,		// fixed length string (16 Byte)
										 AdsVersion*	pVersion		// client buffer to store server version
										 );

__attribute__ ((visibility ("default")))
int32_t  AdsSyncWriteControlReqEx( int32_t			port,			// Ams port of ADS client
									  AmsAddr*			pServerAddr,	// Ams address of ADS server
									  uint16_t	adsState,		// index group in ADS server interface 
									  uint16_t	deviceState,	// index offset in ADS server interface
									  uint32_t		length,			// count of bytes to write
									  void*				pData			// pointer to the client buffer        
									  );

__attribute__ ((visibility ("default")))
int32_t  AdsSyncReadStateReqEx( int32_t port,					// Ams port of ADS client
								  AmsAddr*			pServerAddr,	// Ams address of ADS server
								  uint16_t*	pAdsState,		// pointer to client buffer
								  uint16_t*	pDeviceState	// pointer to the client buffer        
								  );							          


__attribute__ ((visibility ("default")))
int32_t  AdsSyncAddDeviceNotificationReqEx(	int32_t					port,			// Ams port of ADS client
													AmsAddr*				pServerAddr,	// Ams address of ADS ser
													uint32_t			indexGroup,		// index group in ADS server interface
													uint32_t			indexOffset,	// index offset in ADS server interface
													AdsNotificationAttrib*	pNoteAttrib,	// attributes of notification request
													PAdsNotificationFuncEx	pNoteFunc,		// address of notification callback
													uint32_t			hUser,			// user handle
													uint32_t			*pNotification	// pointer to notification handle (return value)
													);
__attribute__ ((visibility ("default")))
int32_t  AdsSyncDelDeviceNotificationReqEx( int32_t				port,					// Ams port of ADS client
													AmsAddr*		pServerAddr,			// Ams address of ADS ser
													uint32_t	hNotification			// notification handle
													);

__attribute__ ((visibility ("default")))
int32_t  AdsSyncSetTimeoutEx(int32_t port,	// Ams port of ADS client
							  int32_t nMs );		// Set timeout in ms

__attribute__ ((visibility ("default")))
int32_t  AdsSyncGetTimeoutEx(int32_t port,	// Ams port of ADS client
							  int32_t *pnMs );		// client buffer to store timeout

__attribute__ ((visibility ("default")))
int32_t  AdsAmsPortEnabledEx(int32_t nPort, int32_t *pbEnabled);

__attribute__ ((visibility ("default")))
int32_t  AdsRouteAdd(const void* ams, const char* ip);

__attribute__ ((visibility ("default")))
int32_t  AdsRouteDel(const void* ams, const char* ip);

__attribute__ ((visibility ("default")))
int32_t  AdsRouteSetLocal(const void* ams);

#ifdef __cplusplus
} // extern "C"
#endif




#endif //__TCADSDLL_H__
