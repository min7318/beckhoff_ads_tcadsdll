OS_NAME ?=$(shell uname)
VPATH = AdsLib
LIBS = -lpthread
LIBS = -lstdc++
LIB_NAME = libtcadsdll.so
LIBA_NAME = adslib.a
OBJ_DIR = obj
OBJSHARED_DIR = objshared
CXX :=gcc 
CXXFLAGS += -std=c++11
CXXFLAGS += -pedantic
CXXFLAGS += -Wall
CXXFLAGS += -Wattributes
CXXFLAGS += -D_GNU_SOURCE
CXXFLAGS += $(ci_cxx_flags)
CPPFLAGS += -I AdsLib/
CPPFLAGS += -I tools/

CPPFLAGS2 = -fPIC
CPPFLAGS2 += -c
CPPFLAGS2 += -fvisibility=hidden

SRC_FILES = AdsDef.cpp
SRC_FILES += AdsLib.cpp
SRC_FILES += AmsConnection.cpp
SRC_FILES += AmsPort.cpp
SRC_FILES += AmsRouter.cpp
SRC_FILES += Log.cpp
SRC_FILES += NotificationDispatcher.cpp
SRC_FILES += Sockets.cpp
SRC_FILES += Frame.cpp
SRC_FILES += TcAdsDll.cpp
OBJ_FILES = $(SRC_FILES:%.cpp=$(OBJ_DIR)/%.o)
OBJSHARED_FILES = $(SRC_FILES:%.cpp=$(OBJSHARED_DIR)/%.o)

ifeq ($(OS_NAME),Darwin)
	LIBS += -lc++
endif

ifeq ($(OS_NAME),win32)
	LIBS += -lws2_32
endif

all: $(LIB_NAME) $(LIBA_NAME)

$(OBJ_DIR):
	mkdir -p $@
	
$(OBJSHARED_DIR) :
	mkdir -p $@

$(OBJ_FILES): | $(OBJ_DIR)
$(OBJ_FILES): $(OBJ_DIR)/%.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS)  $< -o $@

$(OBJSHARED_FILES): | $(OBJSHARED_DIR)
$(OBJSHARED_FILES): $(OBJSHARED_DIR)/%.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $(CPPFLAGS2) $< -o $@

    
$(LIB_NAME): $(OBJSHARED_FILES)
	$(CXX) $(LIBS) -shared  -Wl,-soname=libtcadsdll.so -Wl,--no-undefined -o  $@ $?

$(LIBA_NAME): $(OBJ_FILES)
	$(AR) rvs  $@ $?

AdsLibTest.bin: AdsLibTest/main.cpp 
	$(CXX) $^ $(LIBA_NAME)  $(LIBS)  $(CPPFLAGS) $(CXXFLAGS) -o $@

test: AdsLibTest.bin
	./$<

clean:
	rm -f *.a *.o *.so *.bin AdsLibTest/*.o $(OBJ_DIR)/*.o $$(OBJSHARED_DIR)/*.o

uncrustify:
	uncrustify --no-backup -c tools/uncrustify.cfg AdsLib*/*.h AdsLib*/*.cpp example/*.cpp

prepare-hooks:
	rm -f .git/hooks/pre-commit
	ln -Fv tools/pre-commit.uncrustify .git/hooks/pre-commit
	chmod a+x .git/hooks/pre-commit

.PHONY: clean uncrustify prepare-hooks
